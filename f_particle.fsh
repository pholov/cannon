struct materialProperty {
  vec3 diffuseColor;
  vec3 ambienceColor;
  vec3 specularColor;
  float shinnes;
};
uniform materialProperty u_materialProperty;
uniform sampler2D u_diffuseMap;
uniform float u_colorAlpha;
varying highp vec2 v_texcoord;

void main(void)
{
    gl_FragColor = texture2D(u_diffuseMap, v_texcoord)*vec4(u_materialProperty.diffuseColor,u_colorAlpha);
}
