attribute highp vec4 a_position;
attribute highp vec2 a_texcoord;
uniform highp mat4   u_modelMatrix;
uniform highp mat4   u_viewMatrix;
uniform highp mat4   u_projectionMatrix;
varying highp vec2 v_texcoord;

void main(void)
{
    //mat4 res;
    mat4 mv_matrix = u_viewMatrix * u_modelMatrix;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            mv_matrix[i][j] = (i == j ? 1.0f : 0.0f);
        }
    }
    v_texcoord = a_texcoord;
    gl_Position = u_projectionMatrix * mv_matrix* a_position;
}
