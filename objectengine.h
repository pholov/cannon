#ifndef OBJECTENGINE_H
#define OBJECTENGINE_H

#include <QString>
#include "obj3d.h"
#include "materiallib.h"

class ObjectEngine : public Transformational
{
private:
    QVector<Obj3D *> m_objects;
    MaterialLib m_materialLibrary;

public:
    ObjectEngine(){}
    void draw(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs);
    void translate(const QVector3D &translateVector);
    void rotate (const QQuaternion &rotate);
    void scale (float scaleX,float scaleY,float scaleZ);
    void setGlobalTransform(const QMatrix4x4 &global);
    Obj3D *loadFromObj(const QString &path);
    void addObject(Obj3D *obj);
    void calculateTBN(QVector<VertexData> &vertData);
    Obj3D *getObject(quint32 index);
    Obj3D *getObjectByName(const QString &objectName);    
    void deleteObjectByName(const QString &objectName);
    void deleteObject(Obj3D *object);
};
#endif // OBJECTENGINE_H
