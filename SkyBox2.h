#pragma once
#include "GLHeaders.h"

class SkyBox
{
public:
    SkyBox(GLFuncName *func);
    ~SkyBox();
    void draw(QOpenGLShaderProgram &program, const QMatrix4x4 &model, const QMatrix4x4 &view, const QMatrix4x4 &project, unsigned long time);
    void setSize(float s);
private:
    void initBuffer();
    void initShader();
    void initTexture();
private:
    GLFuncName *m_func;
    QOpenGLBuffer m_arrayBuf;
    QOpenGLShader m_vertexShader;
    QOpenGLShader m_fragmentShader;
    QImage m_images[6];
    QImage m_imagesNight[6];
    quint32 m_cubeTexture;
    quint32 m_cubeTexture2;
    quint32 m_vao;
    float size = 10.0f;
    int m_vertexCount = 0;
};

