#include "emitter.h"

#include <QOpenGLFunctions>

Emitter::Emitter()
{
    m_emissionRate = 80.0f;
    m_deltaEmissionRate = 3.0f;
    m_emissionRadius = 0.0f;
    m_maxEmissionRate = 80.0f;
    m_life = 2.0f;
    m_lifeRange = 0.5f;
    m_size = 0.38f;
    m_sizeRange = 0.25f;
    m_saturation = 0.0f;
    m_alpha = 0.3f;
    m_spread = 0.5f;
    m_gravity = 0.22f;
    m_position = QVector3D(1,2,0);
    m_enable = true;
}


void Emitter::Update(float time, QOpenGLShaderProgram *program, QOpenGLFunctions *funcs)
{
//    if(!m_enable){
//        m_emissionRate = 0.0f;
//    }
//    else {
//        m_emissionRate = 80.0f;
//    }

    if(!m_enable&&m_emissionRate > 0.0f)
        m_emissionRate -= m_deltaEmissionRate;
    if(m_enable&& m_emissionRate < m_maxEmissionRate)
        m_emissionRate = m_maxEmissionRate;
    //qDebug() <<m_emissionRate;
//    if(m_enable)
//        m_elapsedTime += time;

    int numEmissions = int(m_elapsedTime * m_emissionRate);
//    for (int emitted = 0; emitted < numEmissions; emitted++)
//        m_particles.append(
//                    new Particle(frand(m_life - m_lifeRange,
//                                       m_life + m_lifeRange),
//                                frand(m_size - m_sizeRange,
//                                      m_size + m_sizeRange),
//                                 QVector3D(0, -m_gravity, 0),
//                                 QImage(":/particle8.png"),
//                                 QVector3D(0.3f,0.3f,0.3f), m_position,
//                    QVector3D(frand(-m_spread, m_spread), frand(1, 2), frand(-m_spread, m_spread)),m_alpha));

    if (numEmissions > 0)
        m_elapsedTime = 0;

    //update and draw particles
    for (int p = m_particles.length()-1; p >= 0; p--) {
        if (m_particles[p]->update(program,funcs,time)) {
            //delete m_particles[p];
            m_particles.removeAt(p);
        }
    }

}
void Emitter::setTexture(const QImage &texture)
{

}

void Emitter::initParticles()
{
    for (int emitted = 0; emitted < int(m_maxEmissionRate); emitted++)
        m_particles2.append(
                    new Particle(frand(m_life - m_lifeRange,
                                       m_life + m_lifeRange),
                                frand(m_size - m_sizeRange,
                                      m_size + m_sizeRange),
                                 QVector3D(0, -m_gravity, 0),
                                 ":/particle8.png",
                                 QVector3D(0.2f,0.2f,0.2f), m_position,
                                 QVector3D(frand(-m_spread, m_spread), frand(1, 2), frand(-m_spread, m_spread)),m_alpha));
}

void Emitter::shoot()
{
    m_particles = m_particles2;
    foreach (Particle * p, m_particles) {
        p->reset();
    }
}

float Emitter::frand(float start, float end)
{
    //generate between 0 and 1
    float num = float(rand()) / float(RAND_MAX);
    return start + (end - start) * num;
}

void Emitter::enable()
{
     m_enable = true;
}

void Emitter::disable()
{
    m_enable = false;
}

bool Emitter::isEnable()
{
    return m_enable;
}
Emitter::~Emitter()
{

}
