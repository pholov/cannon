#ifndef PARTICLE_H
#define PARTICLE_H

#include "transformational.h"

#include <QObject>
#include <QQuaternion>
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include "obj3d.h"

class QOpenGLTexture;

class Particle : public QObject
{
    Q_OBJECT
public:
    explicit Particle(QObject *parent = nullptr);
    Particle(float life, float size, QVector3D acceliration, const QString &texture, QVector3D color, QVector3D position, QVector3D velocity, float alpha);
    ~Particle();
signals:

public slots:

private:
    QQuaternion m_rotate;
    QVector3D m_translate;
    QVector3D m_startVelocity;
    QVector3D m_scale;
    QVector3D m_velocity;
    QVector3D m_acceleration;
    QMatrix4x4 m_globalTransform;
    QVector3D m_color;
    Obj3D *quad;
    QOpenGLBuffer m_quadVert;
    QOpenGLBuffer m_quadIndex;
    float m_totalLife;
    float m_curLife;
    float m_size;
    float m_alpha;

public:
    bool update(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs, float elapsedTime);
    void reset();
};

#endif // PARTICLE_H
