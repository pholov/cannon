#pragma once
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLContext>
#ifdef _DEBUG
#include <QOpenGLDebugLogger>
#endif

#define GLFuncName QOpenGLFunctions_3_3_Core
