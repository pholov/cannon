#include "materiallib.h"
#include "material.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>

MaterialLib::MaterialLib()
{
    //добавляем материал по умолчанию
    Material *m = new Material;
    m->setMtlName("My_MTL_Default");
    m->setShinnes(96.0f);
    m->setDiffuseColor(QVector3D(1.0f,1.0f,1.0f));
    m->setAmbienceColor(QVector3D(1.0f,1.0f,1.0f));
    m->setSpecularColor(QVector3D(1.0f,1.0f,1.0f));
    addMaterial(m);
}

void MaterialLib::addMaterial(Material *material)
{
    if(material==nullptr) return;
    for (int i =0;i<m_materials.size();i++) {
     if(m_materials[i]==material)
         return;
    }
    m_materials.append(material);
}
//получить материал с заданым именем
Material *MaterialLib::getMaterialByName(const QString &materialName)
{
    foreach(Material *m,m_materials)
         if(m->mtlName()==materialName) return m;
    return nullptr;
}
//получить материал с заданым индексом
Material *MaterialLib::getMaterialByIndex(quint32 index)
{
    if(index > m_materials.size()) return nullptr;
    else return m_materials[index];
}
//кол-во материалов
quint32 MaterialLib::countMaterials()
{
    m_materials.size();
}
//очистить библиотеку
void MaterialLib::clearLib()
{
    foreach (Material *m, m_materials) {
        delete m;
    }
    m_materials.clear();
}
//загрузка материала из файла
void MaterialLib::loadMaterialsFromFile(const QString &filename)
{
    QFile mtlFile(filename);
    if(!mtlFile.open(QIODevice::ReadOnly)){//читаем файл, если не смогли вывод ошибки в дебаг
        qDebug()<<"can't open mtl_file"<<filename;
        return;
    }    
    QFileInfo inf(filename);//загружаем информацию о файле, нужно чтобы получить абсолютный путь к папке с файлом
                            //по идее тамже должны лежать текстуры
    QTextStream inputStream(&mtlFile);
    Material * mtl = nullptr;
    while (!inputStream.atEnd()) {

       QString line = inputStream.readLine();//читаем новую строку
       QStringList list = line.trimmed().split(" ");//разбиваем строку на подстроки пробелом
       if(list[0]=="newmtl")//нашли новый материал
       {
           addMaterial(mtl);//добавили предыдущий
           mtl = new Material;//создали новый
           mtl->setMtlName(list[1]);//назвали
       }else if(list[0]=="Ns")//блик
       {
           mtl->setShinnes(list[1].toFloat());
       }else if(list[0]=="Ka")//фоновый цвет
       {
           mtl->setAmbienceColor(
                       QVector3D(list[1].toFloat(),
                       list[2].toFloat(),
                   list[3].toFloat()));
       }else if(list[0]=="Kd")//рассеяный свет
       {
           mtl->setDiffuseColor(
                       QVector3D(list[1].toFloat(),
                       list[2].toFloat(),
                   list[3].toFloat()));
       }else if(list[0]=="Ks")//отраженноый свет
       {
           mtl->setSpecularColor(
                       QVector3D(list[1].toFloat(),
                       list[2].toFloat(),
                   list[3].toFloat()));
       }else if(list[0]=="map_Kd")//карта текстуры (diffuseMap)
       {
           //составляем строку из имени текстуры и абсолютного пути к папке с mtl файлом
           mtl->setDiffuseMap(QString("%1/%2").arg(inf.absolutePath()).arg(list[1]));
       }else if(list[0]=="map_Bump")//карта нормалей (normalMap)
       {
           //составляем строку из имени текстуры и абсолютного пути к папке с mtl файлом
           mtl->setNormalMap(QString("%1/%2").arg(inf.absolutePath()).arg(list[1]));
       }
    }
    addMaterial(mtl);//добавляем последний материал
    mtlFile.close();//закрываем файл
}


