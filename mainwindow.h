#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QOpenGLWidget>
#include <GL/gl.h>
#include <QTimer>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QQuaternion>
#include "objectengine.h"
#include "emitter.h"
#include "GLHeaders.h"
#include "SkyBox2.h"


class QOpenGLTexture;
class Obj3D;
class Camera3D;
class SkyBox;
class QOpenGLFramebufferObject;
class Light;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;


};

class MyWindow: public QOpenGLWidget, public GLFuncName
{
    Q_OBJECT
private:
    QTimer m_timer;

protected:
    void initShaders();
    Obj3D * initFloor(float width,
                   float heigth,
                   float depht,
                   QString dm=nullptr,//diffuseMap
                   QString nm=nullptr);//normalMap



public:
    MyWindow();
    void initializeGL();
    void resizeGL(int nWidth, int nHeight);
    void paintGL();
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);
    void keyPressEvent(QKeyEvent *e);

    QVector2D m_mouse_position;
    QMatrix4x4 m_projectionMatrix;//матрица проекций
    QMatrix4x4 m_projectionLightMatrix;
    QOpenGLShaderProgram m_programm;// шейдерная программа
    QOpenGLShaderProgram m_programmDepth;//шейдерная программа для карты теней
    QOpenGLShaderProgram m_programmSkyBox;// шейдерная программа для SkyBox
    QOpenGLShaderProgram m_programmParticle;
    QQuaternion m_rotation; // вращение камеры
    ObjectEngine *m_objects;
    ObjectEngine * m_objLoader;
    Emitter *m_emitter;

    QOpenGLFramebufferObject *m_depthBuffer;
    quint32 m_fbHeight;
    quint32 m_fbWidth;

    //SkyBox * m_skybox;
    SkyBox *m_skyBox = nullptr;
    Camera3D * m_camera;

    bool m_isShooting = false;
    Obj3D * m_ball;    
    const QVector3D m_startBallVelocity = {0.6f, 0.3f, 0.0f};
    QVector3D m_ballVelocity = {0.5f, 0.5f, 0.0f};
    QVector3D m_g = {0.0f, -0.0098f, 0.0f};

     Light *m_light[10];
     GLuint m_lightCount = 0;
    ~MyWindow();

public slots:
   void timerTick();

};

#endif // MAINWINDOW_H
