﻿#include "box.h"
#include <QImage>
#include <QOpenGLFunctions>
Box::Box(QOpenGLFunctions *f) : funcs(f)
{
    init();
}

void Box::Draw(const QOpenGLShaderProgram &program)
{

    funcs->glDepthMask(GL_FALSE);
    VAO.bind();
    funcs->glActiveTexture(GL_TEXTURE0);
    funcs->glUniform1i(funcs->glGetUniformLocation(program.programId(), "skybox"), (GLuint)0);
    funcs->glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    funcs->glDrawArrays(GL_TRIANGLES, 0, 36);
    funcs->glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    VBO.bind();
    funcs->glDepthMask(GL_TRUE);
}

void Box::init()
{
    initBuf();
    initTexture();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}
void Box::initTexture()
{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    int width, height;
    QImage images[] = {
        QImage(":/box/lostatseaday/lostatseaday_right.jpg").convertToFormat(QImage::Format_RGB888),
        QImage(":/box/lostatseaday/lostatseaday_left.jpg").convertToFormat(QImage::Format_RGB888),
        QImage(":/box/lostatseaday/lostatseaday_top.jpg").mirrored().convertToFormat(QImage::Format_RGB888),
        QImage(":/box/lostatseaday/lostatseaday_bottom.jpg").convertToFormat(QImage::Format_RGB888),
        QImage(":/box/lostatseaday/lostatseaday_back.jpg").convertToFormat(QImage::Format_RGB888),
        QImage(":/box/lostatseaday/lostatseaday_front.jpg").convertToFormat(QImage::Format_RGB888),
    };
    width = images[0].width();
    height = images[0].height();

    for (int i = 0; i < 6; i++) {
        funcs->glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, (void *)images[i].bits());
    }
    funcs->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}
GLfloat skyboxVertices[] = {
    // Positions
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f,  1.0f
};

void Box::initBuf()
{

    VAO.create();
    VAO.bind();

    VBO.create();
    VBO.setUsagePattern(QOpenGLBuffer::StaticDraw);
    VBO.bind();
    VBO.allocate(&skyboxVertices, sizeof(skyboxVertices));
    funcs->glEnableVertexAttribArray(VBO.bufferId());
    funcs->glVertexAttribPointer(VBO.bufferId(), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *)0);
    VBO.release();
    VAO.release();
}

Box::~Box(){
    delete funcs;
}
