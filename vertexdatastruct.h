#ifndef VERTEXDATASTRUCT_H
#define VERTEXDATASTRUCT_H

#include <QVector2D>
#include <QVector3D>

struct VertexData
{
    VertexData(){}
    VertexData(QVector3D position,
               QVector2D textureCoord,
               QVector3D normal):
        vd_position(position),
        vd_textureCoord(textureCoord),
        vd_normal(normal)
    {}
    QVector3D vd_position;
    QVector2D vd_textureCoord;
    QVector3D vd_normal;
    QVector3D vd_tangent;
    QVector3D vd_bitangent;
};
#endif // VERTEXDATASTRUCT_H
