﻿#ifndef BOX_H
#define BOX_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

class QOpenGLFunctions;

class Box
{
public:
    Box(QOpenGLFunctions *funcs);
    void Draw(const QOpenGLShaderProgram & program);
    ~Box();
protected:
    void init();
private:
    void initTexture();
    void initBuf();
    QOpenGLBuffer VBO;
    QOpenGLVertexArrayObject  VAO;
    GLuint texture;
    QOpenGLFunctions *funcs;
};

#endif // BOX_H
