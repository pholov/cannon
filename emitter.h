#ifndef EMITTER_H
#define EMITTER_H
#include "particle.h"
#include <QImage>
#include <QVector3D>


class Emitter
{
public:
    Emitter();
    ~Emitter();
    void Update(float time, QOpenGLShaderProgram *program, QOpenGLFunctions *funcs);
    void setTexture(const QImage &texture);
    void initParticles();
    void shoot();
    float frand(float start = 0, float end = 1);

    void enable();
    void disable();
    bool isEnable();

private:
    bool m_enable;
    QList<Particle *> m_particles;
    QList<Particle *> m_particles2;
        float
                m_maxEmissionRate,
                m_deltaEmissionRate,
                m_emissionRate,
                m_emissionRadius,
                m_life,
                m_lifeRange,
                m_size,
                m_sizeRange,
                m_saturation,
                m_alpha,
                m_spread,
                m_gravity,
                m_elapsedTime;
    QVector3D   m_position;
};

#endif // EMITTER_H
