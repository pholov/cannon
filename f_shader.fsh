struct materialProperty {
  vec3 diffuseColor;
  vec3 ambienceColor;
  vec3 specularColor;
  float shinnes;
};

struct lightProperty
{
    vec3 ambienceColor;
    vec3 diffuseColor;
    vec3 specularColor;
    vec4 position;
    vec4 direction;
    float cutoff;
    int type;// dir =0; point =1; spot = 2;
    bool isEnable;
};

uniform highp sampler2D u_diffuseMap;
uniform highp sampler2D u_normalMap;
uniform highp sampler2D u_shadowMap;
uniform highp float u_lightPower;
uniform highp vec4 u_specularColor;
uniform materialProperty u_materialProperty;
uniform bool u_isUseingDiffuseMap;
uniform bool u_isUseingNormalMap;
uniform int u_lightCount;
uniform lightProperty u_lightProperty[10];

varying highp vec4 v_position;
varying highp vec2 v_texcoord;
varying highp vec3 v_normal;
varying highp mat3 v_tbnMatrix;
varying highp mat4 v_viewMatrix;

lightProperty v_lightProperty[10];
highp vec3 usingNormal;
varying highp vec4 v_positionLightMatrix;

float SampleShadowMap(sampler2D map, vec2 coords, float compare){
    vec4 v = texture2D(map, coords);
    float value = v.x *255.0+(v.y *255.0+(v.z *255.0 +v.w)/255.0)/255.0;
    return step(compare,value);
}

float SampleShadowMapLinear(sampler2D map, vec2 coords, float compare,vec2 texelSize){
    vec2 pixelPos = coords/texelSize + 0.5;
    vec2 fractPart = fract(pixelPos);
    vec2 startTexel = (pixelPos - fractPart) * texelSize;
    float blTexel = SampleShadowMap(map,startTexel,compare);
    float brTexel = SampleShadowMap(map,startTexel+vec2(texelSize.x,0),compare);
    float tlTexel = SampleShadowMap(map,startTexel+vec2(0,texelSize.y),compare);
    float trTexel = SampleShadowMap(map,startTexel+texelSize,compare);

    float mixLeft = mix(blTexel,tlTexel,fractPart.y);
    float mixRight = mix(brTexel,trTexel,fractPart.y);

    return mix (mixLeft,mixRight,fractPart.x);
}
float SampleShadowMapPCF(sampler2D map, vec2 coords, float compare,vec2 texelSize){

    float result = 0.0;
    for (float y = -2.0;y<2.0; y+=1.0)
        for (float x = -2.0;x<2.0; x+=1.0)
        {
            vec2 offset = vec2(x,y)*texelSize;
            result += SampleShadowMapLinear(map,coords+offset,compare,texelSize);
        }
    return result/9.0;
}

float CalcShadowAmount(sampler2D map, vec4 initialShadowCoords){
    vec3 tmp = v_positionLightMatrix.xyz / v_positionLightMatrix.w;
    tmp = tmp * vec3(0.5) + vec3(0.5);
    highp float offset = 2.0;
    offset*=dot(v_normal,v_lightProperty[0].direction.xyz);
    return  SampleShadowMapPCF(map, tmp.xy,tmp.z*255.0 +offset, vec2(1.0/1024.0));

}

void main(void)
{
    int lightCount = 10;
    if(u_lightCount<10) lightCount = u_lightCount;
    for(highp int i = 0; i<lightCount; i++){
        v_lightProperty[i].ambienceColor = u_lightProperty[i].ambienceColor;
        v_lightProperty[i].diffuseColor = u_lightProperty[i].diffuseColor;
        v_lightProperty[i].specularColor = u_lightProperty[i].specularColor;
        v_lightProperty[i].cutoff = u_lightProperty[i].cutoff;
        v_lightProperty[i].direction = v_viewMatrix * u_lightProperty[i].direction;
        v_lightProperty[i].position = v_viewMatrix * u_lightProperty[i].position;
        v_lightProperty[i].type = u_lightProperty[i].type;
    }

    highp vec4 resultColor = vec4(0.0,0.0,0.0,0.0);
    highp vec4 eyePosition = vec4(0.0,0.0,0.0,1.0);
    vec4 diffMatColor =  texture2D(u_diffuseMap, v_texcoord);
    usingNormal = v_normal;
    if(u_isUseingNormalMap)usingNormal = normalize(texture2D(u_normalMap, v_texcoord).rgb) * 2.0 -1.0;
    vec3 eyeVect = normalize(v_position.xyz - eyePosition.xyz);
    if(u_isUseingNormalMap)eyeVect = normalize(v_tbnMatrix * eyeVect);
    vec3 lightVect;
    for(int i = 0; i<lightCount; i++){
        if(u_lightProperty[i].isEnable){
        highp vec4 resultLightColor = vec4(0.0,0.0,0.0,0.0);
        if(v_lightProperty[i].type==0){//если направленный
                lightVect = normalize(v_lightProperty[i].direction.xyz);
            }else{
                lightVect = normalize(v_position-v_lightProperty[i].position).xyz;
                if(v_lightProperty[i].type==2){//если прожектор
                    float angle = acos(dot(v_lightProperty[i].direction.xyz,lightVect));
                    if(angle>v_lightProperty[i].cutoff)
                        lightVect = vec3(0.0,0.0,0.0);
                }
            }
        if(u_isUseingNormalMap)lightVect = normalize(v_tbnMatrix * lightVect);
        vec3 reflectLight = normalize(reflect(lightVect, usingNormal));
        float len = length(v_position.xyz - eyePosition.xyz);
        float specularFactor = u_materialProperty.shinnes;
        float ambientFactor = 0.1;
        if(u_isUseingDiffuseMap == false) diffMatColor = vec4(u_materialProperty.diffuseColor,1.0);
        vec4 diffColor = diffMatColor * u_lightPower *
                max(0.0,
                    dot (usingNormal, -lightVect));//(1.0+0.25*pow(len,2)));
        resultLightColor += diffColor *  vec4(v_lightProperty[i].diffuseColor,1.0);
        vec4 ambientColor = ambientFactor * diffMatColor;
        resultLightColor += ambientColor * vec4(u_materialProperty.ambienceColor,1.0)*vec4(v_lightProperty[i].ambienceColor,1.0);
        vec4 specularcolor = u_specularColor * u_lightPower *
                pow(max(0.0,
                    dot(reflectLight,-eyeVect)), specularFactor);///(1.0+0.25*pow(len,2));
        resultLightColor += specularcolor* vec4(u_materialProperty.specularColor,1.0) *vec4(v_lightProperty[i].specularColor,1.0);;

        resultColor += resultLightColor;
        }
    }
        highp float shadowCoef = CalcShadowAmount(u_shadowMap,v_positionLightMatrix);
        shadowCoef +=0.16;
        if(shadowCoef>1.0) shadowCoef = 1.0;

    gl_FragColor = resultColor *shadowCoef;
}
