#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector3D>
#include <QVector2D>
#include <QImage>
#include <QMouseEvent>
#include <QOpenGLContext>
#include <QImageReader>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include "camera3d.h"
#include "obj3d.h"
#include "objectengine.h"
#include "material.h"
#include "light.h"


MainWindow::~MainWindow()
{
    delete ui;
}
//инициализация шейдеров
//шейдеры написаны на GLSL (OpenGL Shading Language)
//в данной работе используются шейдеры двух типов: вершинные *.vsh и фрагментные (пиксельные) *.fsh
//вершинные предназначены для расчета позиции вершин
//а пиксельные отвечаю за цвет пикселей на экране
void MyWindow::initShaders()
{
    // эта пара шейдеров отвечает за отрисовку объектов в сцене
    if(!m_programm.addShaderFromSourceFile(QOpenGLShader::Vertex,":/v_shader.vsh"))
        close();
    if(!m_programm.addShaderFromSourceFile(QOpenGLShader::Fragment,":/f_shader.fsh"))
        close();
    if(!m_programm.link())
        close();/*
    //следующая пара отвечает за отрисовку скайбокса
    if(!m_programmSkyBox.addShaderFromSourceFile(QOpenGLShader::Vertex,":/skybox.vsh"))
        close();
    if(!m_programmSkyBox.addShaderFromSourceFile(QOpenGLShader::Fragment,":/skybox.fsh"))
        close();
    if(!m_programmSkyBox.link())
        close();*/
    //эта пара шейдеров для реализации карты теней (расчитыват глубину)
    if(!m_programmDepth.addShaderFromSourceFile(QOpenGLShader::Vertex,":/depth.vsh"))
        close();
    if(!m_programmDepth.addShaderFromSourceFile(QOpenGLShader::Fragment,":/depth.fsh"))
        close();
    if(!m_programmDepth.link())
        close();
    //эта пара шейдеров для реализации карты теней (расчитыват глубину)
    if(!m_programmParticle.addShaderFromSourceFile(QOpenGLShader::Vertex,":/v_particle.vsh"))
        close();
    if(!m_programmParticle.addShaderFromSourceFile(QOpenGLShader::Fragment,":/f_particle.fsh"))
        close();
    if(!m_programmParticle.link())
        close();
}


Obj3D *MyWindow::initFloor(float width,float heigth,float depht, QString dm, QString nm)
{
    float width_div_2 = width/2.0f;//т.к. пол находится в центре
    float depht_div_2 = depht/2.0f;//считаем координаты точек, деля ширину и глубину пополам
    QVector<VertexData> vertexes;
    //добавляем вершины
    vertexes.append(VertexData(QVector3D(width_div_2, heigth,depht_div_2),  QVector2D(0,32),  QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(width_div_2, heigth,-depht_div_2), QVector2D(0,0),   QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(-width_div_2,heigth,depht_div_2),  QVector2D(32,32), QVector3D(0,1,0)));
    vertexes.append(VertexData(QVector3D(-width_div_2,heigth,-depht_div_2), QVector2D(32,0),  QVector3D(0,1,0)));

    QVector<uint> indexes;
    //индексы (прямоугольник рисуется из двух треугольников, т.к. Obj3D отрисовывает модели треугольниками)
    // поэтому иедексов 6
    indexes.append(0);
    indexes.append(1);
    indexes.append(2);
    indexes.append(2);
    indexes.append(1);
    indexes.append(3);
    //********* настройка материала ********
    Material *m = new Material();
    if(dm!=nullptr) m->setDiffuseMap(dm);
    if(nm!=nullptr) m->setNormalMap(nm);
    m->setDiffuseColor({0.8f,0.8f,0.8f});
    m->setAmbienceColor({0.8f,0.8f,0.8f});
    m->setSpecularColor({0.0f,0.0f,0.0f});
    //**************************************
    return new Obj3D(vertexes,indexes,m);//возвращаем созданный объект
}
unsigned long int time_ms = 0, time_start = 0;
void MyWindow::timerTick()
{
    if(m_isShooting){
        m_ballVelocity += m_g;
        m_ball->translate(m_ballVelocity);
    }
    time_ms+=16;

    int time = time_ms% 24000;
    if(time_ms%24000<12001){
        m_light[0]->setDiffuseColor(m_light[0]->diffuseColor()+QVector3D(0.00053333333f,0.00053333333f,0.0f));
    }else{
         m_light[0]->setDiffuseColor(m_light[0]->diffuseColor()-QVector3D(0.00053333333f,0.00053333333f,0.0f));
    }
    if(m_isShooting&&time_ms-time_start>=2000){
        m_objects->deleteObject(m_ball);
        m_isShooting = false;
    }
}

MyWindow::MyWindow()
{
    m_emitter = new Emitter();
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(update()));
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(timerTick()));
    m_timer.start(16);
    m_fbHeight = 1024;
    m_fbWidth = 1024;//размеры буфера глубины
    m_camera = new Camera3D();//создаем камеру и перемещаем её
    m_camera->rotate(QQuaternion::fromAxisAndAngle(QVector3D(1.0f,0,0),25));
    m_camera->translate(QVector3D(0.0f,-4.0f,-10.0f));
    m_projectionLightMatrix.setToIdentity();//матрица проецирования для тени
    m_projectionLightMatrix.ortho(-90, 90, -90,90,-90,90);//в данном случае проекция ортогональная
    m_light[0] = new Light();//направленный источник света
    m_lightCount++;//посчитали источник света
    m_light[0]->setDirection(-QVector4D(10.0f,10.0f,10.0f,0.0f));//направление
    m_light[0]->setDiffuseColor({0.2f, 0.2f, 0.4f});//цвет
    m_light[1] = new Light(Light::Spot);//прожектор


}

void MyWindow::initializeGL()
{
    initializeOpenGLFunctions();
    auto functions = context()->versionFunctions<QOpenGLFunctions_3_3_Core>();
    m_skyBox =  new SkyBox(functions);
    m_emitter->initParticles();
    glShadeModel(GL_SMOOTH);
    glClearDepth(1);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_PERSPECTIVE_CORRECTION_HINT);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glClearColor(0.0f,0.0f,0.0f,1.0f);//задаем цвет очистки
    glEnable(GL_DEPTH_TEST);//очищаем буфер глубины, он нам понадобится
    glEnable(GL_CULL_FACE);//включаем отображение
    glCullFace(GL_BACK);//только лицивых граней
    initShaders();//загрузка шейдеров

    m_objects =new ObjectEngine;//создаем обработчик объектов

//    QImage dm (":/156.JPG");//карта поглащения света ("текстура")
//    QImage nm (":/156_norm.JPG");//карта нормалей
    m_objects->addObject(initFloor(100.0f,0.0f,100.0f,
              ":/156.JPG",":/156_norm.JPG"));//создаем пол и добавляем его в обработчик объектов
    m_objects->loadFromObj(":/cannon/cannon/Old_mortar.obj");
    m_ball = m_objects->loadFromObj(":/cannon/ball.obj");
    m_ball->scale(0.16f, 0.16f, 0.16f);
    m_ball->translate({1.0f, 2.0f, -0.1f});
    m_objects->deleteObject(m_ball);

    QString pref = ":/blue/blue/bkg1_";//начало пути к текстурам скайбокса; blue папка в папке с программой, bkg1_ начало имени текстуры
    /*m_skybox = new SkyBox(99999,
                          pref+"front.png",pref+"back.png",
                          pref+"left.png", pref+"right.png"
                         ,pref+"top.png",  pref+"bot.png");//создаем скайбокс*/
    m_depthBuffer = new QOpenGLFramebufferObject(m_fbWidth,m_fbHeight,QOpenGLFramebufferObject::Depth);//создаем буфер глубины

}
void MyWindow::resizeGL(int nWidth, int nHeight)
{
    float aspect = nWidth/(nHeight ? float(nHeight) : 1);//считаем отношение сторон
    //приводим проекционную матрицу к единичной
    m_projectionMatrix.setToIdentity();
    //масштабирум окно
    m_projectionMatrix.perspective(45/*угол поля зрения в направлении OY*/,
                                   aspect/*отношение сторон в направлении OX*/,
                                   0.01f/*насколько близко отрисовывать*/
                                   ,100000.0f/*насколько далеко отрисовывать*/);

}
void MyWindow::paintGL()
{

    //отрисовка во фрэймбуффер (для карты теней)
    m_depthBuffer->bind();//биндим буфер, мы его уже объявляли
    glViewport(0,0,m_fbWidth,m_fbHeight);//устанавливаем прот просмотра (от (0,0 до 1024,1024)
    glClear(GL_DEPTH_BUFFER_BIT |GL_COLOR_BUFFER_BIT); //очищаем глубину и цвет
    m_programmDepth.bind();//биндим шейдерную программу
    //******************* устанавливаем значения uniform переменных ********************
    m_programmDepth.setUniformValue("u_projectionLightMatrix",m_projectionLightMatrix);
    m_programmDepth.setUniformValue("u_shadowLightMatrix",m_light[0]->lightMatrix());
    //**********************************************************************************

    //в цикле отрисовываем каждый объект в сцене (кроме скайбокса и камеры, разумеется),
    //чтобы получить тень
    m_objects->draw(&m_programmDepth, context()->functions());
    m_programmDepth.release();//освобождаем программу
    m_depthBuffer->release();//очищаем буфер глубины


    //отрисовка на экран
    glViewport(0,0,width(),height());//устанавливаем нормальный порт просмотра в координатах окна
  //  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    //m_programmSkyBox.bind();//биндим программу отрисовки скайбокса
    //рисуем скайбокс
    //m_camera->draw();
    //m_programmSkyBox.setUniformValue("u_projectionMatrix",m_projectionMatrix);
    //m_skybox->draw(&m_programmSkyBox,context()->functions());
    //m_programmSkyBox.release();

    m_skyBox->draw(m_programmSkyBox, {}, QMatrix4x4(m_camera->viewMatrix().normalMatrix()), m_projectionMatrix, time_ms);


    GLuint texture = m_depthBuffer->texture();//получаем id текстуры
    //делаем текстуру активной
    context()->functions()->glActiveTexture(GL_TEXTURE6);//используем контекст, т.к. по какой-то причине эта ф-ция у нас не определена
    glBindTexture(GL_TEXTURE_2D, texture);//биндим текстуру на GL_TEXTURE6

    m_programm.bind();//биндим основную программу
    m_camera->draw(&m_programm);//ставим камеру
    //******************* устанавливаем значения uniform переменных ********************
    m_programm.setUniformValue("u_projectionLightMatrix",m_projectionLightMatrix);
    m_programm.setUniformValue("u_shadowMap",GL_TEXTURE6-GL_TEXTURE0);//нужно передать id, а он считается какGL_TEXTUREX-GL_TEXTURE0
    m_programm.setUniformValue("u_projectionMatrix",m_projectionMatrix);
    m_programm.setUniformValue("u_shadowLightMatrix", m_light[0]->lightMatrix());
    m_programm.setUniformValue("u_lightPower",3.0f);
    m_programm.setUniformValue("u_specularColor",QVector4D(1.0f,1.0f,1.0f,1.0f));
   //**********************************************************************************
   //******************* устанавливаем значения uniform переменных для разных источников света ********************
    int lCount = 0;
    for(int i = 0; i<m_lightCount;i++){
        m_programm.setUniformValue(QString("u_lightProperty[%1].ambienceColor").arg(i).toLatin1().data(),m_light[i]->ambienceColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].diffuseColor").arg(i).toLatin1().data(),m_light[i]->diffuseColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].specularColor").arg(i).toLatin1().data(),m_light[i]->specularColor());
        m_programm.setUniformValue(QString("u_lightProperty[%1].position").arg(i).toLatin1().data(),m_light[i]->position());
        m_programm.setUniformValue(QString("u_lightProperty[%1].direction").arg(i).toLatin1().data(),m_light[i]->direction());
        m_programm.setUniformValue(QString("u_lightProperty[%1].cutoff").arg(i).toLatin1().data(),m_light[i]->cutoff());//угол отсечения
        m_programm.setUniformValue(QString("u_lightProperty[%1].type").arg(i).toLatin1().data(),m_light[i]->type());//тип
        m_programm.setUniformValue(QString("u_lightProperty[%1].isEnable").arg(i).toLatin1().data(),m_light[i]->isEnable);//включен ли
        lCount++;
    }
    //*************************************************************************************************************
    m_programm.setUniformValue("u_lightCount", lCount);//кол-во источников света

    //рисуем объекты в сцену
    m_objects->draw(&m_programm, context()->functions());
    m_programm.release();//освобождаем программу

    glDisable(GL_DEPTH_TEST);
    m_programmParticle.bind();
    m_camera->draw(&m_programmParticle);
    m_programmParticle.setUniformValue("u_projectionMatrix",m_projectionMatrix);
    m_emitter->Update(float(m_timer.interval())/ 1000.0f, &m_programmParticle, context()->functions());
    m_programmParticle.release();
    glEnable(GL_DEPTH_TEST);

}


//нажатия кнопок мыши
void MyWindow::mousePressEvent(QMouseEvent *e)
{
    if(e->buttons()==Qt::LeftButton){//если нажали левую кнопку мыши
        m_mouse_position = QVector2D( e->localPos()); //сохраняем координаты
    e->accept();
    }
}

//перемещения мыши
void MyWindow::mouseMoveEvent(QMouseEvent *e)
{
 if(e->buttons()!=Qt::LeftButton) return;//если не нажата левая кнопак, выходим
 QVector2D diff = QVector2D( e->localPos()) - m_mouse_position; //считаем сколько прошла мышь на экране
 m_mouse_position = QVector2D( e->localPos());//обновляем данные о позиции мыши

 float angleX = diff.y()/5.0f;//угол поворота по X определяем как y вектора перемещения мыши
 float angleY = diff.x()/5.0f;//для Y наооборот
 //на 5 делим чтобы камера не дергалась

 m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,angleX));//поворачиваем камеру по X
 m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,angleY));//поворачиваем камеру по Y
 update();//перерисовываем окно
}
//колесеко мыши
void MyWindow::wheelEvent(QWheelEvent *e)
{
    float scaleFactor = 0.3f;//на сколько приближать(отдалять)
    if(e->delta()>0)//если колесеко крутим на себя
    {
        m_camera->translate({0,0,scaleFactor});//приближаем
    }else if(e->delta()<0)//если колесеко крутим от себя
    {
       m_camera->translate({0,0,-scaleFactor});//отдаляем
    }
    update();//перерисовываем окно
}


//нажатие клавишь на клавиатуре
void MyWindow::keyPressEvent(QKeyEvent *e)
{
    float angle = 2.0f;//угол поворота камеры
    int k = e->key();//какую клавишу нажали
    switch (k) {
    case Qt::Key_W: {m_camera->translate(QVector3D(0,0,1));break;}//перемещаемся вперед
    case Qt::Key_S: {m_camera->translate(QVector3D(0,0,-1));break;}//перемещаемся назад
    case Qt::Key_A: {m_camera->translate(QVector3D(1,0,0));break;}//перемещаемся вправо
    case Qt::Key_D: {m_camera->translate(QVector3D(-1,0,0));break;}//перемещаемся влево
    case Qt::Key_E:{m_camera->translate(QVector3D(0,1,0));break;}//перемещаемся вниз
    case Qt::Key_Q:{m_camera->translate(QVector3D(0,-1,0));break;}//перемещаемся вверх
    case Qt::Key_F:{//возвращаем камеру в исходное положение
        m_camera->clearMovments();
        //как в конструкторе
        m_camera->rotate(QQuaternion::fromAxisAndAngle(QVector3D(1.0f,0,0),25));
        m_camera->translate(QVector3D(0.0f,-4.0f,-10.0f));
        break;}
    case Qt::Key_1: {break;}
    case Qt::Key_2: {break;}
    case Qt::Key_3: {break;}
    case Qt::Key_4: {break;}
    case Qt::Key_5: {m_light[0]->isEnable=!m_light[0]->isEnable;break;} // вкл/выкл направленый свет
    case Qt::Key_6: {m_light[1]->isEnable=!m_light[1]->isEnable;break;}// вкл/выкл прожектор
    case Qt::Key_7: {m_light[2]->isEnable=!m_light[2]->isEnable;break;}// вкл/выкл точечный свет
    case Qt::Key_8: {
        if(time_ms%24000<20000&&time_ms%24000>8000&&!m_isShooting){
            time_start = time_ms;
            m_emitter->shoot();
            m_emitter->isEnable()?m_emitter->disable():m_emitter->enable();
            m_ballVelocity = QVector3D(m_startBallVelocity);
            m_objects->addObject(m_ball);
            m_ball->setPosition({1.0f, 2.0f, -0.1f});
            m_isShooting = true;
        }
        break;
    }
        //повороты камеры стрелками
    case Qt::Key_Left:
    {
        m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,-angle));
        break;
    }
    case Qt::Key_Right:
    {
        m_camera->rotateY(QQuaternion::fromAxisAndAngle(0,1.0f,0,angle));
        break;
    }    case Qt::Key_Up:
    {
        m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,-angle));
        break;
    }
    case Qt::Key_Down:
    {
        m_camera->rotateX(QQuaternion::fromAxisAndAngle(1.0f,0,0,angle));
        break;
    }
    case Qt::Key_F1:{
        this->window()->showFullScreen();
        break;
    }
    case Qt::Key_F2:{
        this->window()->showNormal();
        break;
    }
    default:;
    }
    update();//перерисовываем
}

MyWindow::~MyWindow()
{
    delete m_camera;
    delete m_objects;
}
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
