#include "SkyBox2.h"

SkyBox::SkyBox(GLFuncName *func)
    : m_func(func)
    , m_vertexShader(QOpenGLShader::Vertex)
    , m_fragmentShader(QOpenGLShader::Fragment)
{
    //setSize(4999.0f);

    initShader();
    initBuffer();
    initTexture();
}

SkyBox::~SkyBox()
{
    m_arrayBuf.destroy();
}

void SkyBox::initBuffer()
{
    size = 1.0f;
    GLfloat skyboxVertices[] = {
        // Positions
        -size,  size, -size,
        -size, -size, -size,
        size, -size, -size,
        size, -size, -size,
        size,  size, -size,
        -size,  size, -size,

        -size, -size,  size,
        -size, -size, -size,
        -size,  size, -size,
        -size,  size, -size,
        -size,  size,  size,
        -size, -size,  size,

        size, -size, -size,
        size, -size,  size,
        size,  size,  size,
        size,  size,  size,
        size,  size, -size,
        size, -size, -size,

        -size, -size,  size,
        -size,  size,  size,
        size,  size,  size,
        size,  size,  size,
        size, -size,  size,
        -size, -size,  size,

        -size,  size, -size,
        size,  size, -size,
        size,  size,  size,
        size,  size,  size,
        -size,  size,  size,
        -size,  size, -size,

        -size, -size, -size,
        -size, -size,  size,
        size, -size, -size,
        size, -size, -size,
        -size, -size,  size,
        size, -size,  size
    };
    m_vertexCount = sizeof(skyboxVertices)/sizeof(skyboxVertices[0]);
    m_arrayBuf.create();

    m_func->glGenVertexArrays(1, &m_vao);
    m_func->glBindVertexArray(m_vao);

    m_arrayBuf.bind();
    m_arrayBuf.allocate(skyboxVertices, sizeof(skyboxVertices));
    const int positionLocation = 0;
    m_func->glEnableVertexAttribArray(positionLocation);
    m_func->glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, sizeof (GLfloat) * 3, nullptr);

    m_func->glBindVertexArray(0);
}

void SkyBox::setSize(float s)
{
    size = s;
}

void SkyBox::initShader()
{
    const auto vShader = u8R"(#version 330 core
layout(location = 0) in vec3 position;
uniform mat4 mvp;
out vec3 vTexCoord;
void main()
{
    vTexCoord = position;
    vec4 pos = mvp * vec4(position, 1.0);
    gl_Position = pos.xyww;
}
)";
    const auto fShader = R"(#version 330 core
uniform samplerCube skyBox;
uniform samplerCube skyBox2;
uniform float blendFactor;
in vec3 vTexCoord;
out vec4 outColor;
void main()
{
    vec4 texture1 = texture(skyBox, vTexCoord);
    vec4 texture2 = texture(skyBox2, vTexCoord);
    outColor = mix(texture1, texture2, blendFactor);
}
)";
    if (!m_vertexShader.compileSourceCode(vShader)) {
        qWarning() << m_vertexShader.log();
    }
    if (!m_fragmentShader.compileSourceCode(fShader)) {
        qWarning() << m_fragmentShader.log();
    }
}

void SkyBox::initTexture()
{
    m_images[0] = QImage(":/assets/right.png").convertToFormat(QImage::Format_RGB888);
    m_images[1] = QImage(":/assets/left.png").convertToFormat(QImage::Format_RGB888);
    m_images[2] = QImage(":/assets/top.png").convertToFormat(QImage::Format_RGB888);
    m_images[3] = QImage(":/assets/bottom.png").convertToFormat(QImage::Format_RGB888);
    m_images[4] = QImage(":/assets/back.png").convertToFormat(QImage::Format_RGB888);
    m_images[5] = QImage(":/assets/front.png").convertToFormat(QImage::Format_RGB888);
    m_func->glGenTextures(1, &m_cubeTexture);
    m_func->glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubeTexture);
    for (unsigned int i = 0; i < 6; ++i) {
        m_func->glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i
                             , 0
                             , GL_RGB
                             , m_images[i].width()
                                 , m_images[i].height()
                                 , 0
                             , GL_RGB
                             , GL_UNSIGNED_BYTE
                             , m_images[i].bits()
                             );
    }
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


    m_imagesNight[0] = QImage(":/assets/nightRight").convertToFormat(QImage::Format_RGB888);
    m_imagesNight[1] = QImage(":/assets/nightLeft.png").convertToFormat(QImage::Format_RGB888);
    m_imagesNight[2] = QImage(":/assets/nightTop.png").convertToFormat(QImage::Format_RGB888);
    m_imagesNight[3] = QImage(":/assets/nightBottom.png").convertToFormat(QImage::Format_RGB888);
    m_imagesNight[4] = QImage(":/assets/nightBack.png").convertToFormat(QImage::Format_RGB888);
    m_imagesNight[5] = QImage(":/assets/nightFront.png").convertToFormat(QImage::Format_RGB888);
    m_func->glGenTextures(1, &m_cubeTexture2);
    m_func->glBindTexture(GL_TEXTURE_CUBE_MAP, m_cubeTexture2);
    for (unsigned int i = 0; i < 6; ++i) {
        m_func->glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i
                             , 0
                             , GL_RGB
                             , m_imagesNight[i].width()
                                 , m_imagesNight[i].height()
                                 , 0
                             , GL_RGB
                             , GL_UNSIGNED_BYTE
                             , m_imagesNight[i].bits()
                             );
    }
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    m_func->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}
void SkyBox::draw(QOpenGLShaderProgram &program, const QMatrix4x4 &model, const QMatrix4x4 &view, const QMatrix4x4 &project, unsigned long time)
{
    //initBuffer();
    //initTexture();
    m_arrayBuf.bind();
    const int positionLocation = 0;
    m_func->glEnableVertexAttribArray(positionLocation);
    m_func->glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, sizeof (GLfloat) * 3,nullptr);
    m_func->glDepthFunc(GL_LEQUAL);
    program.removeAllShaders();
    program.addShader(&m_vertexShader);
    program.addShader(&m_fragmentShader);
    if (!program.link()) {
        qWarning() << program.log();
        return;
    }
    if (!program.bind()) {
        qWarning() << program.log();
        return;
    }
    time %= 24000;
            int texture1;
            int texture2;
            float blendFactor;
            if( time < 5000){
                texture1 = m_cubeTexture2;
                texture2 = m_cubeTexture2;
                blendFactor = (float(time) - 0.0f)/(5000.0f - 0.0f);
            }else if(time >= 5000 && time < 8000){
                texture1 = m_cubeTexture2;
                texture2 = m_cubeTexture;
                blendFactor = (float(time) - 5000.0f)/(8000.0f - 5000.0f);
            }else if(time >= 8000 && time < 21000.0f){
                texture1 = m_cubeTexture;
                texture2 = m_cubeTexture;
                blendFactor = (float(time) - 8000.0f)/(21000.0f - 8000.0f);
            }else{
                texture1 = m_cubeTexture;
                texture2 = m_cubeTexture2;
                blendFactor = (float(time) - 21000.0f)/(24000.0f - 21000.0f);
            }
    m_func->glBindVertexArray(m_vao);
    m_func->glActiveTexture(GL_TEXTURE0);
    m_func->glBindTexture(GL_TEXTURE_CUBE_MAP, texture1);
    m_func->glActiveTexture(GL_TEXTURE1);
    m_func->glBindTexture(GL_TEXTURE_CUBE_MAP, texture2);
    program.setUniformValue("mvp", project * view * model);
    program.setUniformValue("skyBox", 0);
    program.setUniformValue("skyBox2", 1);
    program.setUniformValue("blendFactor", blendFactor);
    m_func->glDrawArrays(GL_TRIANGLES, 0, m_vertexCount);
    m_func->glDepthFunc(GL_LESS);
    m_arrayBuf.release();
}
