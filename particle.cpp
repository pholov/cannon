#include "particle.h"
#include "material.h"
#include "vertexdatastruct.h"
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>


Particle::Particle(QObject *parent) : QObject(parent),
    m_scale(QVector3D(1.0f, 1.0f,1.0f))
{

}

Particle::Particle(float life, float size, QVector3D acceliration,  const QString &texture, QVector3D color, QVector3D position, QVector3D velocity, float alpha):
     m_scale(QVector3D(1.0f, 1.0f,1.0f)), m_acceleration(acceliration),m_color(color), m_totalLife(life),m_curLife(life), m_size(size), m_velocity(velocity), m_alpha(alpha)
{
    m_size = size;
    m_translate = position;
    m_startVelocity = velocity;
    //m_texture =  new QOpenGLTexture(texture.mirrored());
//    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);//Nearest - выбирает ближайший пиксель
//    m_texture->setMagnificationFilter(QOpenGLTexture::Linear);//Linear - выбирает линейную интерполяцию ближайших пикселей
//    m_texture->setWrapMode(QOpenGLTexture::Repeat);//если текстурные координаты не лежат в промежутке от (0,0) до (1,1) мод Repeat заполнаяет выбраной текстурой
//    m_quadVert.create();
//    m_quadIndex.create();
    //initQuad(m_size, m_quadVert, m_quadIndex);
    QVector<VertexData> vertexes;
    QVector<GLuint>     indexes;
    vertexes.append(VertexData(QVector3D( size,  size, 0),  QVector2D(1,1), QVector3D(0,0,1)));
    vertexes.append(VertexData(QVector3D(-size,  size, 0),  QVector2D(1,0), QVector3D(0,0,1)));
    vertexes.append(VertexData(QVector3D( size, -size, 0),  QVector2D(0,1), QVector3D(0,0,1)));
    vertexes.append(VertexData(QVector3D(-size, -size, 0),  QVector2D(0,0), QVector3D(0,0,1)));
    indexes.append(0);
    indexes.append(1);
    indexes.append(2);
    indexes.append(2);
    indexes.append(1);
    indexes.append(3);
    Material *m = new Material();
    m->setDiffuseMap(texture);
    m->setDiffuseColor(color);
    m->setAmbienceColor({0.8f,0.8f,0.8f});
    m->setSpecularColor({0.0f,0.0f,0.0f});
    quad = new Obj3D(vertexes, indexes,m);
    quad->translate(position);
}



bool Particle::update(QOpenGLShaderProgram *program, QOpenGLFunctions *funcs, float elapsedTime)
{
    m_velocity   += m_acceleration * elapsedTime;
    //m_translate  += m_velocity * elapsedTime;
    funcs->glEnable(GL_BLEND);
    funcs->glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    const float fadeTime = 0.2f;
    if (m_totalLife - m_curLife < fadeTime) {
       quad->setColor(m_color.x(), m_color.y(), m_color.z(), (m_totalLife - m_curLife) / fadeTime * m_alpha);
    }
    else if (m_curLife < 1.0f) {
        quad->setColor(m_color.x(), m_color.y(), m_color.z(), m_curLife * m_alpha);
    }
    else {
        quad->setColor(m_color.x(), m_color.y(), m_color.z(), m_alpha);
    }
    quad->translate(m_velocity * elapsedTime);
    quad->draw(program, funcs);
    funcs->glDisable(GL_BLEND);
    m_curLife    -= elapsedTime;

    return m_curLife <= 0.0f;
}

void Particle::reset()
{
    m_curLife = m_totalLife;
    m_velocity = m_startVelocity;
    quad->setTranslate(m_translate);
}

Particle::~Particle()
{
    delete quad;
}
